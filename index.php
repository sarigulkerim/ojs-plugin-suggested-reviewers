<?php

/**
 * @defgroup plugins_generic_suggestedReviewers Plugin
 */
 
/**
 * @file plugins/generic/suggestedReviewers/index.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_suggestedReviewers
 *
 */

require_once('SuggestedReviewersPlugin.inc.php');
return new SuggestedReviewersPlugin();